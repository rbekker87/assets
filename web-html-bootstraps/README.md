## Bootstrap
* [Preview: nav-black-dropdowns-simple](http://rbekker87.surge.sh/assets/web-html-bootstraps/bootstrap/nav-black-dropdowns-simple.html)
* [Preview: marketing](http://rbekker87.surge.sh/assets/web-html-bootstraps/bootstrap/marketing-bootstrap.html)
* [Preview: boxes-template-w-search](http://rbekker87.surge.sh/assets/web-html-bootstraps/bootstrap/boxes-template-w-search.html)
* [Preview: nav-dropdown-jumbo-search](http://rbekker87.surge.sh/assets/web-html-bootstraps/bootstrap/nav-dropdown-search-1.html)
* [Preview: search-baby-search](http://rbekker87.surge.sh/assets/web-html-bootstraps/bootstrap/search-basic-sbs.html)

## Skeleton
* [Preview: skeleton](http://rbekker87.surge.sh/assets/web-html-bootstraps/skeleton/basic-skeleton.html)

## Statics
* [Preview: basic-search](http://rbekker87.surge.sh/assets/web-html-bootstraps/statics/search-basic.html)
* [Preview: happy-birthday-baloons](https://objects.ruanbekker.com/assets/html/john-sarah/index.html)

