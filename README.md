# Home for Assets [![Awesome](https://s13.postimg.org/a8csw1sx3/home.png)](https://gitlab.com/rbekker87/assets/blob/master/README.md)

*Static content for Websites*

## Table of Contents

- [css](#awesome-python)
- [img](#environment-management)
- [js](#package-management)

## Bootstrap Sources:

- [getbootstrap](www.getbootstrap.com)
- [skeleton](ww.getskeleton.com)

## Bootstrap Examples (Ext):

- [Justified Nav v3](https://getbootstrap.com/docs/3.3/examples/justified-nav/)
- [Justified Nav v4](https://v4-alpha.getbootstrap.com/examples/justified-nav/)
